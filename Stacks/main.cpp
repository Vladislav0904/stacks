#pragma once
#include "Stack.h"
#include "StackArray.h"
#include "StackArray.cpp"
#include "BoundedQueue.h"
#include "BoundedQueue.cpp"


bool checkBalanceBrackets(const std::string& text, const int maxDeep);
void stackArrayTest();
void bracketsTest();
void boundedQueueTest();


int main() {
	stackArrayTest();
	std::cout << "\n";
	bracketsTest();
	std::cout << "\n";
	boundedQueueTest();
	return 0;	
}

bool checkBalanceBrackets(const std::string& text, const int maxDeep) {
	StackArray<char> stk;
	int size = 0;
	for (const char& c : text) {
		if (c == '(' || c == '[' || c == '{') {
			if (size >= maxDeep) {
				return false;
			}
			stk.push(c);
			size++;
		}
		else if (c == ')' || c == ']' || c == '}') {
			if (stk.isEmpty()) {
				return false;
			}
			char top = stk.top();
			if ((c == ')' && top == '(') || (c == ']' && top == '[') || (c == '}' && top == '{')) {
				stk.pop();
				size--;
			}
			else {
				return false;
			}
		}
	}
	return stk.isEmpty();
}

void stackArrayTest() {
	try {
		std::cout << "Trying to create StackArray with wrong size: ";
		StackArray<int> stack(-10);
	}
	catch (const StackArray<int>::WrongStackSize& e) {
		std::cerr << e.what() << "\n";
	}
	StackArray<int> stack(3);
	try {
		stack.push(1);
		stack.push(2);
		stack.push(3);
		std::cout << "Pushing 1 2 3 to stack and printing with pop: ";
		while (!stack.isEmpty()) {
			std::cout << stack.top() << " ";
			stack.pop();
		}
		std::cout << "\n";
		std::cout << "Popping from empty stack: ";
		stack.pop();
	}
	catch (const StackArray<int>::StackUnderflow& msg) {
		std::cerr << msg.what() << "\n";
	}
	try {
		std::cout << "Adding new values and trying to add more elements than allowed: ";
		stack.push(1);
		stack.push(2);
		stack.push(3);
		stack.push(4);
	}
	catch (const StackArray<int>::StackOverflow& msg) {
		std::cerr << msg.what() << "\n";
	}
}

void bracketsTest() {
	std::string text = "hellp() {a[s]r}";
	int maxDeep = 3;
	std::cout << "String is: " << text << "\n";
	std::cout << ((checkBalanceBrackets(text, maxDeep)) ? "Balance is ok\n" : "Balance is not ok\n");
	std::string text2 = "wrong disorder [(])";
	std::cout << "String is: " << text2 << "\n";
	std::cout << ((checkBalanceBrackets(text2, maxDeep)) ? "Balance is ok\n" : "Balance is not ok\n");
}

void boundedQueueTest() {
	try {
		std::cout << "Trying to create BoundedQueue with wrong size: ";
		BoundedQueue<int> queue(-1);
	}
	catch (const BoundedQueue<int>::WrongQueueSize& msg) {
		std::cerr << msg.what() << "\n";
	}
	BoundedQueue<int> queue(5);
	queue.enQueue(10);
	queue.enQueue(20);
	queue.enQueue(30);
	queue.enQueue(40);
	queue.enQueue(50);

	try {
		std::cout << "Trying to overflow BoundedQueue: ";
		queue.enQueue(60);
	}
	catch (const BoundedQueue<int>::QueueOverflow& msg) {
		std::cerr << msg.what() << "\n";
	}
	std::cout << "Printing queue with dequeue: " << "\n";
	while (!queue.isEmpty()) {
		std::cout << queue.deQueue() << "\n";
	}
	try {
		std::cout << "Trying to deQueue from empty queue: ";
		queue.deQueue();
	}
	catch (const BoundedQueue<int>::QueueUnderflow& msg) {
		std::cerr << msg.what() << "\n";
	}
}