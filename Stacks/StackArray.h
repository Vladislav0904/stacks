#pragma once
#include "Stack.h"
#include <iostream>
#include <exception>


template <class T>
class StackArray : public Stack<T>
{
public:
	StackArray(size_t size = 100);
	StackArray(const StackArray<T>& src) = delete;
	StackArray& operator=(const StackArray<T>& src) = delete;
	StackArray& operator=(StackArray<T>&& src) {};
	virtual ~StackArray() { delete[] array_; }
	void push(const T& e) override;
	T& pop() override;
	size_t size() { }
	size_t top() { return array_[top_]; };
	bool isEmpty() { return top_ == 0; };
	class WrongStackSize : public std::exception {
	public:
		WrongStackSize() : reason_("WrongStackSize") {};
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
	class StackOverflow : public std::exception
	{
	public:
		StackOverflow() : reason_("Stack Overflow") {}
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
	class StackUnderflow : public std::exception
	{
	public:
		StackUnderflow() : reason_("Stack Underflow") {}
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
private:
	T* array_;
	size_t top_;
	size_t size_;
	void swap(StackArray<T>& src);
};

