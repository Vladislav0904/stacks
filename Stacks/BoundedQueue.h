#pragma once
#include "Queue.h"
#include <exception>
#include <iostream>

template <class T>
class BoundedQueue : public Queue<T>
{
private:
    T* arr;
    int front, rear;
    int capacity;
    int count;
public:
    BoundedQueue(int size);
    ~BoundedQueue() { delete[] arr; }
    void enQueue(const T& e);
    T deQueue();
    bool isEmpty() { return count == 0;}
	class WrongQueueSize : public std::exception {
	public:
		WrongQueueSize() : reason_("WrongQueueSize") {};
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
	class QueueOverflow : public std::exception
	{
	public:
		QueueOverflow() : reason_("Queue Overflow") {}
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
	class QueueUnderflow : public std::exception
	{
	public:
		QueueUnderflow() : reason_("Queue Underflow") {}
		const char* what() const { return reason_; }
	private:
		const char* reason_;
	};
};