#include "BoundedQueue.h"
#include <exception>

template <class T>
BoundedQueue<T>::BoundedQueue(int size) {
    capacity = size;
    front = 0;
    rear = -1;
    count = 0;
    try {
        arr = new T[size];
    } catch (...) { 
        throw WrongQueueSize();
    }
}
template <class T>
void BoundedQueue<T>::enQueue(const T& e)
{
    if (count == capacity)
    {
        throw QueueOverflow();
    }
    else
    {
        rear = (rear + 1) % capacity;
        arr[rear] = e;
        count++;
    }
}
template <class T>
T BoundedQueue<T>::deQueue()
{
    if (isEmpty())
    {
        throw QueueUnderflow();
    }
    else
    {
        T temp = arr[front];
        front = (front + 1) % capacity;
        count--;
        return temp;
    }
}